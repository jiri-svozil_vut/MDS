package mds.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

import static mds.project.FilePaths.DASH_DIRECTORY;
import static mds.project.FilePaths.SEPARATOR;

@Controller
public class WebController {

    private ProjectResourceComponent handler;

    @Autowired
    public WebController(ProjectResourceComponent h) {
        this.handler = h;
    }

    @GetMapping("index")
    public String index(){
        return "index";
    }

    @PostMapping("video")
    public String video(Model model,
                        @RequestParam(required = false, defaultValue = FilePaths.VIDEO_FROM_URL) String url,
                        @RequestParam(required = false, defaultValue = "1000") String height){
        model.addAttribute("video_url", url);
        model.addAttribute("video_height", height);
        return "video";
    }

    @GetMapping(value = {"/dash/{stream}/{file}"})
    public void streaming(
            @PathVariable String file,
            @PathVariable String stream,
            HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        File STREAM_FILE = new File(DASH_DIRECTORY + stream + SEPARATOR + file);
        request.setAttribute(ProjectResourceComponent.ATTR_FILE, STREAM_FILE);
        handler.handleRequest(request, response);
    }

    @RequestMapping(value = "player/{stream}")
    public String video(Model model, @PathVariable String stream){
        model.addAttribute("video_url", stream);
        return "player";
    }

    @RequestMapping(value = "streamcollection")
    public String streamCollection(Model model) throws IOException {
        StreamLibrary library = new StreamLibrary(DASH_DIRECTORY, "mpd");
        model.addAttribute("library", library);
        return "streamcollection";
    }

    @RequestMapping(value = "playercontrols/{stream}")
    public String playerControls(Model model, @PathVariable String stream){
        String url = "/dash/" + stream + "/manifest.mpd";
        model.addAttribute("url", url);
        return "playercontrols";
    }

}
