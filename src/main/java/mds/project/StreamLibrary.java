package mds.project;

import org.apache.commons.io.file.SimplePathVisitor;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

import static mds.project.FilePaths.SEPARATOR;


public class StreamLibrary extends ArrayList<String> {

    public StreamLibrary(String directory, String keepSuffix) throws IOException {
        this.addAll(discoverFiles(Path.of(directory), keepSuffix));
    }

    private static List<String> discoverFiles(Path direcroty, String keepSuffix) throws IOException {
        List<String> files = new ArrayList<>();
        Files.walkFileTree(direcroty, new SimplePathVisitor() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                String filePath = String.valueOf(file);
                if (filePath.endsWith(keepSuffix)) {
                    // Rozdíl mezi původní cestou a cesto k soubrou... mělo by vypadnout něco ve stylu: /DASH-BIG_BUCK_BUNNY
                    String difference = file.getParent().toString().replace(direcroty.toString(), "");
                    // Odstranění SEPARATORU v mém případě "/"... výsledek: DASH-BIG_BUCK_BUNNY
                    files.add(difference.replace(SEPARATOR, ""));
                }
                return super.visitFile(file, attrs);
            }
        });
        return files;
    }
}
